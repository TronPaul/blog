Chat Etiquette
==============

:date: 2020-11-27 08:28
:tags: culture
:slug: chat-etiquette
:authors: Mark McGuire

What is Chat?
-------------

Chat is an asynchronous form of communication using text and images. Some other forms of
asynchronous communication include text messages, emails, or letters. In these forms of
communication the sender does not know when someone will read or reply to the message.
In contract, having a face to face conversation, a phone call, or a video chat are all
forms of synchronous communication. In practice the usual time to response for a chat message
is shorter than other forms of asynchronous communication.

Chat messages are either sent directly to a person or group of people, also known as a direct message,
or to a channel, sometimes called a room. A direct message can only be read by the recipients. A
message to a channel can be read by anyone who joins the channel.

Some messages trigger a notification, also known as a ping or @, to users. Typically notifications trigger
a noise or vibration along with a pop-up to get a user's attention. In most chat applications
a direct message will notify the users that it is sent to. In many chat applications a message containing an @ symbol
followed by a username will notify that user. This is useful when you want your message to be seen
by everyone in the channel and also want a quick response from the user whose username you pinged.

Many chat applications will also allow you to notify all users in a channel that are online, via
@here, all users in a channel, @channel, or all users, @everyone.

A ping in effect makes the reading and the response to the message more synchronous. When
someone gets a notification, they're very likely to drop what they are doing to read the message
and respond.

Best Practices
--------------

Different forms of communication have different sets of best practices. Some of the rules
I try to follow and appreciate when chatting are:

#. Only trigger a notification (aka ping, or @) for someone or a group when the message in the notification is

   #. Applicable: it is relevant to that person or all the people in the group being pinged
   #. Actionable: it requires an action or response by the person or group pinged
   #. Time Sensitive: it requires resolution at this very moment

#. Write all that I want to say in one message
   
   #. Bad: Hello
   #. Good: Hello, I'm having an issue with X. Something seems to be going on with Y.

#. Keep messages concise and to the point
#. Be blunt, but polite

What's the deal with @?
-----------------------

Remember chat is asynchronous. 

It is a great way to have a conversation or discussion when not interrupting people is important.
With chat people can choose when to read and respond to messages. If something requires their
full attention, they can put off checking for new messages until after they are finished or when
they need a break from what they are working on.

A notification interrupts those notified and breaks their concentration. This is desirable when
the reason for the notification is critical and time sensitive to everyone notified, but if that is
not the case, then people will had their time wasted and may be less likely to check a ping quickly
in the future. The greater the number of people pinged, the more more time wasted by a bad ping.
This is why @here and its siblings are dangerous tools as they have the potential of frustrating many
people by having their concentration broken.

Example: Small Question, Big Channel
++++++++++++++++++++++++++++++++++++

John is working on a document. The due date is not near and there is still ample
time to finish it. He realizes he needs a specific graphic for the document and that
it should already exist on a shared drive somewhere. However, it's not documented where and John cannot
find it. There's other work that could be done on the document, so John isn't blocked,
but he'll need that image before he can send the document out.

John switches to his chat application and opens the channel of the team that owns
the image. There are 100 people in the channel: Some of them are on the team that owns the
image, some of them joined because they interact with that team often, and some joined because
they previous, or currently have a question. There's no one person who's responsible for answering questions so John
sends an @here: "Hey, does anyone know where image XYZ is? I checked on the wiki and
in the J and G drives, but couldn't find it @here."

70 of those 100 people are online so 70 people get notifications on their laptops and only
30 of those 70 are team members. Sally is one of those team members. She sees John's message
and is pretty sure where the image is. She responds to John: "John, that image is on the K
drive. Should be ABC/2020/234/final/XYZ.jpg"

John checks that location, thanks Sally and continues work on his document. Problem solved!
But was it solved well? Let's walk through what could have been done differently.

Review
______

John did a great job trying to solve his problem on his own and making it clear what he needed
when he asked his question. The mistake John made was using @here in such a populated channel.

Not only did the notification hit 40 people who weren't on the team that owned the image and
most likely wouldn't be able to help John, but also interrupted Sally and her teammates. Sally
may have been in the midst of planning a new graphic for next quarter's document and with the
interruption, now has to get back in the creative mindset of graphic planning.

Another issue is that only one team member was needed to help John. Bob also saw the message, 
but he was slower to respond than Sally and took a few minutes to find where the image was.
Not only was Bob interrupted from what he was working on, but his effort was wasted as Sally
already helped.

Hello
-----

Hello. It's a polite way of starting a conversation. It also contains no information about
what the converstation will entail. In chat, if you were to send just "Hello", anyone
reading that message would be wondering what you will send next. If no follow up message
came quickly they might ask what it was you needed. Another possibility is no one immediately
sees that message and you're waiting for a response. Either is not ideal.

Every message should have enough information for people to read and react appropriately. You'd
be very confused by a letter or email that just contained "Hello" and chat is no different.
Your messages should be polite, but should also have all the information someone needs to
respond or follow up.

And That's Chat
---------------

Even before the crazy town that is 2020, I've considered chat to be really important. It's been
around for 47 years (since Talkomatic in 1973), but only recently has it grown to be such an
important part of how people communicate. Hopefully these practices help you to be more effective
chatting or help explain why certain chat behaviors can be frustrating.

Thanks and don't @ me :)

Yubikey + WSL2 = 👍
===================

:date: 2020-03-29 09:00
:tags: WSL2, Yubikey
:slug: yubikey-wsl2
:authors: Mark McGuire

.. role:: strike
    :class: strike

Some time ago I bought a Yubikey_ and have enjoyed using it as a second factor and
an SSH key (signing my git commits is also a plus). It's been pretty easy to set
up everywhere with one exception: Windows and `Windows Subsystem for Linux 2`_ (WSL2).

.. _Yubikey: https://www.yubico.com/
.. _Windows Subsystem for Linux 2: https://docs.microsoft.com/en-us/windows/wsl/wsl2-index

I prefer Linux for development and putzing around the internet, but I also enjoy
playing video games on mouse and keyboard. This means I need a Windows machine [#]_.

WSL2_ is currently in Windows preview builds and is an improvement on the first version of WSL.
It basically lets you run an Ubuntu system within Windows and provides some interop.

.. _WSL2: https://docs.microsoft.com/en-us/windows/wsl/wsl2-index

Getting a Yubikey setup in Windows was fairly straightforward, but not entirely simple.
Getting a Yubikey setup in WSL2_ was an exercise in guess and check guide following, but
I've finally gotten a setup that works and have written a guide for myself for
:strike:`if` when my desktop desides to give up the ghost.

Windows Yubikey Setup Guide
---------------------------

#. `Install Chocolatey`_ (you could install the following Windows bits yourself, but
   I found Chocolatey to make things much easier)

   .. _Install Chocolatey: https://chocolatey.org/install

#. In a Administrator Powershell run::

    $ choco install -y git gpg4win putty.install

   This will install the necessary bits for your Yubikey to work in Windows.

#. In a normal Powershell run::

    $ mkdir $env:APPDATA/gnupg
    $ Add-Content -Path $env:APPDATA/gnupg/gpg-agent.conf -Value "enable-putty-support`r`nenable-ssh-support"

   .. _step 4 in the Windows guide:
#. Import your public GPG key & trust it

   a) If you've put your GPG public key on a public URL and set the URL on your Yubikey_::
 
       $ gpg --card-edit
       fetch
       $ gpg --edit-key YOUR_KEY
       trust
       5

   b) If you just have a ``.asc`` file as your public key::

       $ gpg --import PATH_TO_ASC
       $ gpg --edit-key YOUR_KEY
       trust
       5

   This will make your public key fully trusted by GPG.

#. Setup your Git configuration::

      $ git config --global core.sshcommand 'plink -agent'
      $ git config --global gpg.program "c:\Program Files (x86)\GnuPG\bin\gpg.exe"
      $ git config --global user.email "YOUR_EMAIL"
      $ git config --global user.name "YOUR_NAME"

   a) if you want to sign all your Git commits::

       $ git config --global user.signingkey YOUR_KEY
       $ git config --global commit.gpgsign true

WSL2 Yubikey Setup Guide
------------------------

To get GPG and to use your Yubikey_ as your SSH key in WSL2_ you'll need to follow
the wsl2-ssh-pageant_ guide. I'll reproduce it here:

.. _wsl2-ssh-pageant: https://github.com/BlackReloaded/wsl2-ssh-pageant

**WARNING**: forwarding Pageant and GPG from Windows to WSL2_ means that ANYONE who
can SSH into your account in WSL2_ can access your GPG key.

#. From within WSL2_ Ubuntu::

    $ sudo apt install socat

#. Install wsl2-ssh-pageant.exe into WSL2_::

    $ wget https://github.com/BlackReloaded/wsl2-ssh-pageant/releases/download/v1.2.0/wsl2-ssh-pageant.exe -O $HOME/.ssh/wsl2-ssh-pageant.exe

#. Make it executable::

   $ chmod +x $HOME/.ssh/wsl2-ssh-pageant.exe

#. Add the following to your ``~/.profile`` in WSL2_

   a) For SSH to work::

       export SSH_AUTH_SOCK=$HOME/.ssh/agent.sock
       ss -a | grep -q $SSH_AUTH_SOCK
       if [ $? -ne 0 ]; then
           rm -f $SSH_AUTH_SOCK
           setsid nohup socat UNIX-LISTEN:$SSH_AUTH_SOCK,fork EXEC:$HOME/.ssh/wsl2-ssh-pageant.exe >/dev/null 2>&1 &
       fi

   b) For GPG to work::

       export GPG_AGENT_SOCK=$HOME/.gnupg/S.gpg-agent
       ss -a | grep -q $GPG_AGENT_SOCK
       if [ $? -ne 0 ]; then
           rm -rf $GPG_AGENT_SOCK
           setsid nohup socat UNIX-LISTEN:$GPG_AGENT_SOCK,fork EXEC:"$HOME/.ssh/wsl2-ssh-pageant.exe --gpg S.gpg-agent" >/dev/null 2>&1 &
       fi

#. Add your GPG key to your trusted keys in WSL2_ (same as `step 4 in the Windows guide`_
   but run from within WSL2_)

   a) If you've put your GPG public key on a public URL and set the URL on your Yubikey_::
 
       $ gpg --card-edit
       fetch
       $ gpg --edit-key YOUR_KEY
       trust
       5

   b) If you just have a ``.asc`` file as your public key::

       $ gpg --import PATH_TO_ASC
       $ gpg --edit-key YOUR_KEY
       trust
       5

I hope this makes Yubikey_ and WSL2_ easy to use. It's really nice that I don't
need to SSH, dual boot, or grab another machine to do development when I'm using
my gaming desktop.

.. [#] It's "doable" to play games on Linux, but definitely more effort and less performant. I
   only have so many yaks I can shave.


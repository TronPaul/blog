#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Mark McGuire'
SITENAME = 'Mark McGuire'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'America/Chicago'

DEFAULT_LANG = 'en'

THEME = 'pelican-simplegrey'

SUMMARY_MAX_LENGTH = 75
DEFAULT_CATEGORY = 'Miscellaneous'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = tuple()

# Social widget
SOCIAL = (('Gitlab', 'https://gitlab.com/TronPaul'),
          ('Github', 'https://github.com/TronPaul'),
          ('LinkedIn', 'https://www.linkedin.com/in/mark-mcguire/'),)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
